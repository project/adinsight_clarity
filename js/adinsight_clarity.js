(function ($, Drupal, drupalSettings, once) {

  Drupal.behaviors.adinsight_clarity = {
    attach: function (context, settings) {
      $(once('adinsight_clarity_behavior', 'body')).each(function () {
        window.adiInit = settings.adinsight_clarity.account || drupalSettings.adinsight_clarity.account;
        (function() {
          var adiSrc = document.createElement("script");
          adiSrc.type = "text/javascript";
          adiSrc.async = true;
          adiSrc.src = ("https:" == document.location.protocol ? "https://static-ssl" : "http://static-cdn") + ".responsetap.com/static/scripts/rTapTrack.min.js";
          var s = document.getElementsByTagName("script")[0];
          s.parentNode.insertBefore(adiSrc, s);
        })();
      });
    }
  };

})(jQuery, Drupal, drupalSettings, once);

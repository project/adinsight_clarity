AdInsight Clarity is call level visitor tracking service which provides insight
into customer activity on your website prior to making a call to you.

This module adds the AdInsight dynamic number generating and tracking script to
a Drupal site and provides a number of methods for a site maintainer to insert a
special markup target for the dynamically generated number.

Exposes:
- A Block
- A filter
- A Token (if you have the token module installed)

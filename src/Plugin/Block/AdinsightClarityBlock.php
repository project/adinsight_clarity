<?php

namespace Drupal\adinsight_clarity\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides the AdinsightClarityBlock block.
 *
 * @Block(
 *   id = "adinsight_clarity_block",
 *   admin_label = @Translation("AdInsight Clarity Telephone Number")
 * )
 */
class AdinsightClarityBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return _adinsight_clarity_build_tag();
  }

}

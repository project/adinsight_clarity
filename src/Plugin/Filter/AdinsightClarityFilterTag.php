<?php

namespace Drupal\adinsight_clarity\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to replace the adinsight telephone tag.
 *
 * @Filter(
 *   id = "adinsight_clarity_filter_tag",
 *   title = @Translation("AdInsight Telephone Tag"),
 *   description = @Translation("Every instance of the special <code><strong>&lt;adinsight /&gt;</strong></code> tag will be replaced with the telephone number markup."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class AdinsightClarityFilterTag extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor for the redirect operations view field.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepare($text, $langcode) {
    return preg_replace('!<adinsight ?/>!', '[adinsight-tag]', $text);
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    $markup = _adinsight_clarity_build_tag();
    $result->setProcessedText(str_replace(
      '[adinsight-tag]',
      $this->renderer->render($markup),
      $text
    ));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('<em>&lt;adinsight /&gt;</em> is replaced with the current telephone tag markup.');
  }

}

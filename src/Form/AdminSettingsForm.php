<?php

namespace Drupal\adinsight_clarity\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * AdInsight Clarity administration UI.
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new AdminSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, RendererInterface $renderer) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'adinsight_clarity_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['adinsight_clarity.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('adinsight_clarity.settings');
    $base = $config->get('base_phone');
    $pool = $config->get('pool');

    $form['account'] = [
      '#type' => 'fieldset',
      '#title' => 'Account Settings',
    ];

    $form['account']['account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AdInsight Clarity Account'),
      '#default_value' => $config->get('account'),
      '#size' => 10,
      '#maxlength' => 10,
      '#required' => TRUE,
      '#description' => $this->t('Enter your <a href="@adinsight">AdInsight Clarity</a> account number', [
        '@adinsight' => 'http://www.adinsight.com/',
      ]),
    ];

    $form['account']['base_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base telephone number'),
      '#default_value' => $base,
      '#size' => 20,
      '#maxlength' => 20,
      '#required' => TRUE,
      '#description' => $this->t('<p>The base number should match the dymanic number format (numbers and spaces)</p><p>The base phone number is inserted into the page and replaced with the dynamicly generated number when the AdInsight script executes. The base phone number will also be seen by users who have JavaScript disabled.</p>'),
    ];

    $form['account']['pool'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dynamic Number Pool ID'),
      '#default_value' => $pool,
      '#size' => 4,
      '#maxlength' => 4,
      '#required' => TRUE,
      '#description' => $this->t('Defines the number pool that the AdInsight script draws dynamic numbers from.'),
    ];

    $form['usage'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Usage'),
    ];

    // Usage instructions can only be shown if the number pool and base phone
    // number fields are already set, so we swap out the usage instructions for
    // a warning until they are set.
    if (!empty($pool) && !empty($base)) {
      $tag = _adinsight_clarity_build_tag();
      $tag = (string) $this->renderer->render($tag);

      $form['usage']['method-filter'] = [
        '#type' => 'item',
        '#title' => $this->t('Filter'),
        '#description' => $this->t('This module enables a custom filter which will automatically replace the special <code>&lt;adinsight /&gt;</code> tag with the AdInsight span tag: <code><strong>@tag</strong></code>', [
          '@tag' => $tag,
        ]),
      ];
      $form['usage']['method-block'] = [
        '#type' => 'item',
        '#title' => $this->t('Block'),
        '#description' => $this->t('This module exposes a block called <em>AdInsight Clarity Telephone Number</em> containing the AdInsight span tag.'),
      ];

      if ($this->moduleHandler->moduleExists('token')) {
        $form['usage']['method-token'] = [
          '#type' => 'item',
          '#title' => $this->t('Token'),
          '#description' => $this->t('This module exposes tokens for each of the settings and a token for the HTML span.'),
        ];
      }
      else {
        $form['usage']['method-token'] = [
          '#type' => 'item',
          '#title' => $this->t('Token'),
          '#description' => $this->t('<p>The Token module is not enabled.</p><p>This module exposes tokens for each of the settings and a token for the HTML span.</p>'),
        ];
      }

      $form['usage']['method-tag'] = [
        '#type' => 'item',
        '#title' => $this->t('Manual HTML'),
        '#description' => $this->t('<p>You can insert the following span tag at each point that you want your phone number to appear:</p><code><strong>@tag</strong></code><p><strong>Note:</strong> If you change the Number Pool or Base Number settings you will need to replace any existing spans in your theme or content.</p>', [
          '@tag' => $tag,
        ]),
      ];
    }
    else {
      $form['usage']['info'] = [
        '#type' => 'item',
        '#title' => '',
        '#description' => $this->t('Please complete the account settings section and save your changes.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check the fields for errors.
    $has_errors = FALSE;

    // Account should be a number.
    if (!preg_match('/^[0-9]{1,}$/', $form_state->getValue('account'))) {
      $form_state->setErrorByName('account', t('Your AdInsight Clarity account must be numeric'));
      $has_errors = TRUE;
    }

    // Base number should match substitution format (numbers and spaces).
    if (!preg_match('/^[0-9 ]{1,}$/', $form_state->getValue('base_phone'))) {
      $form_state->setErrorByName('base_phone', t('The base telephone number must match the dynamic substitution format (numbers and spaces)'));
      $has_errors = TRUE;
    }

    // Pool should be a number.
    if (!preg_match('/^[0-9]{1,}$/', $form_state->getValue('pool'))) {
      $form_state->setErrorByName('pool', t('Your AdInsight Clarity pool ID must be numeric'));
      $has_errors = TRUE;
    }

    // If there are no errors reset the tag builder static.
    if (!$has_errors) {
      drupal_static_reset('_adinsight_clarity_build_tag');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('adinsight_clarity.settings')
      ->set('account', $form_state->getValue('account'))
      ->set('pool', $form_state->getValue('pool'))
      ->set('base_phone', $form_state->getValue('base_phone'))
      ->save();
  }

}

<?php

/**
 * @file
 * Builds placeholder replacement tokens for AdInsight Clarity.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function adinsight_clarity_token_info() {

  $type = [
    'name' => t('AdInsight Clarity'),
    'description' => t('Tokens related to the AdInsight Clarity tracking library.'),
  ];

  $adminsight = [];

  $adinsight['account'] = [
    'name' => t('Account ID'),
    'description' => t('The current AdInsight Clarity account ID.'),
  ];
  $adinsight['pool'] = [
    'name' => t('Number Pool'),
    'description' => t('The ID of the number pool from which to draw dynamic numbers.'),
  ];
  $adinsight['base-number'] = [
    'name' => t('Base Number'),
    'description' => t('The base phone number for non-JavaScript users, replaced by a number from the number pool.'),
  ];
  $adinsight['tag'] = [
    'name' => t('HTML span tag'),
    'description' => t('A completely formatted telephone number span, allowing a token to be used in the theme and replaced with the correct phone number'),
  ];

  return [
    'types' => ['adinsight_clarity' => $type],
    'tokens' => ['adinsight_clarity' => $adinsight],
  ];
}

/**
 * Implements hook_tokens().
 */
function adinsight_clarity_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $sanitize = !empty($options['sanitize']);

  $replacements = [];

  if ($type == 'adinsight_clarity') {
    $config = \Drupal::config('adinsight_clarity.settings');
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'account':
          $value = $config->get('account');
          $replacements[$original] = $sanitize ? Html::escape($value) : $value;
          break;

        case 'pool':
          $value = $config->get('pool');
          $replacements[$original] = $sanitize ? Html::escape($value) : $value;
          break;

        case 'base-number':
          $value = $config->get('base_phone');
          $replacements[$original] = $sanitize ? Html::escape($value) : $value;
          break;

        case 'tag':
          $tag = _adinsight_clarity_build_tag();
          $replacements[$original] = \Drupal::service('renderer')
            ->render($tag);
          break;
      }
    }

    if ($replacements) {
      $bubbleable_metadata->addCacheableDependency($config);
    }
  }

  return $replacements;
}
